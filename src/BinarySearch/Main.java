package BinarySearch;
import java.util.Scanner;
public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner in = new Scanner(System.in);
		System.out.println("Enter length");
		int length = in.nextInt();
		int[] arr = new int[length];
		System.out.println("Enter the number you will search in the  array");
		int x = in.nextInt(); 
		System.out.println("Enter " + length + " numbers");
		for(int i = 0;i<length;i++) {
			arr[i] = in.nextInt();
		}
		
		int[] sortedarr = insertionSort(arr, length, 2);
		
		System.out.println("The index of " + x + " in the sorted array which is ");
		displayArray(sortedarr);
		System.out.println(" is " + search(sortedarr, 0, length-1, x));	
	}
	public static int[] insertionSort(int[] arr,int n,int nOfUnsortedArray) {
		for(int i = nOfUnsortedArray-1;i>0;i--) {
			if(arr[i] < arr[i-1]) {
				int temp = arr[i];
				arr[i] = arr[i-1];
				arr[i-1] = temp;
			}
			else {
				break;
			}
			
		}
		if(nOfUnsortedArray == n) {
			return arr;
		}
		else {
			return insertionSort(arr, n, nOfUnsortedArray + 1); 
		}	
		
	}
	public static int search(int[] arr,int l, int r, int x) {
		if(l < r) {
			int m = (l+r) / 2;
			
			if(x == arr[0]) {
				return 0;
			}
			else if(x == arr[arr.length-1]) {
				return arr.length-1;
				
			}
			else {
				if(x < arr[m]) {
					search(arr, l, m, x);
				}
				if(x > arr[m]) {
					search(arr, m+1, r, x);
				}
				
				if(x == arr[m]) {
					return m;
				}
			}		
		}
		return -1;
		
	}
	public static void displayArray(int[] arr) {
		int length = arr.length;
		for(int i = 0;i<length;i++) {
			if(i == length-1) {
				System.out.print(arr[i]);
			}
			else {
				System.out.print(arr[i] + ",");
			}
		}	
	}
}
